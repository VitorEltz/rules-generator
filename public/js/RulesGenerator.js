const jsonRules = {
    "rules": [
        {
            "id": "1.0",
            "main_op": "source",
            "operation": "",
            "value": "",
            "desc": ""
        }
    ]
};

let formsCounter = 0;

function createNewRule() {
    const typeChoosed = document.querySelector('#ruleTypes').value;
    createForm(typeChoosed);   
}

function createSectionHead(title, body, section) {
    const rowHeadTitle = HtmlGenerator.createTitleRow(title);
    const deleteButton = HtmlGenerator.createDeleteButton();
    const colButton = HtmlGenerator.createCol(6, false);
    colButton.classList.add('text-right');

    deleteButton.addEventListener('click', () => {
        const deleteSection = confirm('Are you sure you want to delete?');

        if (deleteSection) {
            body.removeChild(section);
            calcSubRuleId();
        }
    });

    colButton.appendChild(deleteButton);

    rowHeadTitle.appendChild(colButton);
    return rowHeadTitle;
}

function createSectionHeadRuleMap(body, section) {
    const rowHeadTitle = HtmlGenerator.createRow();
    rowHeadTitle.classList.add('mb-3');

    const h2 = document.createElement('h2');
    h2.classList.add('source-color');
    h2.classList.add('fs-18');
    h2.classList.add('text-center');
    h2.classList.add('text-sm-center');
    h2.classList.add('text-md-center');
    h2.classList.add('text-lg-left');
    h2.classList.add('text-xl-left');
    h2.innerHTML = 'RULE MAP';

    const colTitle = HtmlGenerator.createCol(4);
    colTitle.appendChild(h2);


    const colNewButtons = HtmlGenerator.createCol(null, false);
    colNewButtons.classList.add('col-12');
    colNewButtons.classList.add('col-sm-12');
    colNewButtons.classList.add('col-md-12');
    colNewButtons.classList.add('col-lg-6');
    colNewButtons.classList.add('col-xl-6');
    colNewButtons.classList.add('offset-lg-2');
    colNewButtons.classList.add('offset-xl-2');



    const rowButtons = HtmlGenerator.createRow();
    rowButtons.classList.add('justify-content-between');

    colNewButtons.appendChild(rowButtons);

    const colCheck = HtmlGenerator.createCol(4, false);
    colCheck.classList.add('text-left');
    colCheck.classList.add('pr-lg-0');
    colCheck.classList.add('pr-xl-0');
    const newCheckButton = HtmlGenerator.createSuccessButton('New Check');

    newCheckButton.addEventListener('click', () => {
        const checkSection = HtmlGenerator.createSection();
        checkSection.classList.remove('data-section');
        checkSection.classList.add('check-data');
        const divCol = HtmlGenerator.createCol(12);

        checkSection.appendChild(createCheckForm(checkSection));
        divCol.appendChild(checkSection);
        section.appendChild(divCol);

        calcSubRuleId();
    });

    const colAction = HtmlGenerator.createCol(4, false);
    colAction.classList.add('text-center');
    colAction.classList.add('pl-lg-0');
    colAction.classList.add('pl-xl-0');
    const createActionButton = HtmlGenerator.createSuccessButton('New Action');
    createActionButton.addEventListener('click', () => {
        const actionSection = HtmlGenerator.createSection();
        actionSection.classList.remove('data-section');
        actionSection.classList.add('action-data');
        const divCol = HtmlGenerator.createCol(12);

        actionSection.appendChild(createActionForm(actionSection));
        divCol.appendChild(actionSection);
        section.appendChild(divCol);

        calcSubRuleId();
    });

    colAction.appendChild(createActionButton);
    colCheck.appendChild(newCheckButton);
    // const ghostCol = HtmlGenerator.createCol(6);


    const colDeleteButton = HtmlGenerator.createCol(4, false);
    colDeleteButton.classList.add('text-right');
    const deleteButton = HtmlGenerator.createDeleteButton();

    deleteButton.addEventListener('click', () => {
        const deleteSection = confirm('Are you sure you want to delete?');

        if (deleteSection) {
            body.removeChild(section);
            calcSubRuleId();
        }
    });

    
    colDeleteButton.appendChild(deleteButton);

    rowButtons.appendChild(colCheck);
    rowButtons.appendChild(colAction);
    rowButtons.appendChild(colDeleteButton);

    rowHeadTitle.appendChild(colTitle);
    rowHeadTitle.appendChild(colNewButtons);
    // rowHeadTitle.appendChild(colDeleteButton);
    return rowHeadTitle;
}

function createActionForm(section) {
    const container = HtmlGenerator.createCol(12);
    const rowHead = HtmlGenerator.createRow();
    const row2 = HtmlGenerator.createRow();
    rowHead.classList.add('mb-3');

    const col1Row1 = HtmlGenerator.createCol(6, false);
    const col2Row1 = HtmlGenerator.createCol(6, false);
    col2Row1.classList.add('text-right');

    const h2 = document.createElement('h2');
    h2.classList.add('source-color');
    h2.classList.add('fs-18');
    h2.classList.add('text-left');
    h2.innerHTML = 'ACTION';
    col1Row1.appendChild(h2);

    const deleteActionSection = HtmlGenerator.createDeleteButton();
    deleteActionSection.addEventListener('click', () => {
        const confirmation = confirm('Are you sure you want to delete action?');
        if (confirmation) {
            section.remove();
            calcSubRuleId();
        }
    });
    col2Row1.appendChild(deleteActionSection);

    rowHead.appendChild(col1Row1);
    rowHead.appendChild(col2Row1);


    const col1Row2 = HtmlGenerator.createIdText();
    const col2Row2 = HtmlGenerator.createCol(5);
    const col3Row2 = HtmlGenerator.createCol(5);

    const optionsSelect1 = [
        { text: 'Notification', isSelected: true },
        { text: 'Email', value: 'email' },
        { text: 'Alert', value: 'alert' }
    ];
    const inputSelectNotification = HtmlGenerator.createSelect('Notification Alert', 'selectOptionsAlertNotification', optionsSelect1);
    col2Row2.appendChild(inputSelectNotification);

    const optionsSelect2 = [
        { text: 'Alerts', isSelected: true },
        { text: 'Any', value: 'any' },
        { text: 'Alert 1', value: 'alert_1' },
        { text: 'Alert 2', value: 'alert_2' },
        { text: 'Alert 3', value: 'alert_3' },
        { text: 'Alert 4', value: 'alert_4' }
    ];
    const inputSelectAlert = HtmlGenerator.createSelect('Alert Type', 'selectOptionsAlertsType', optionsSelect2);
    col3Row2.appendChild(inputSelectAlert);

    row2.appendChild(col1Row2);
    row2.appendChild(col2Row2);
    row2.appendChild(col3Row2);

    const row3 = HtmlGenerator.createRow();
    const col1Row3 = HtmlGenerator.createCol(7);
    const checkInput = HtmlGenerator.createInput('Var Name: ', 'inputActionVarName');
    col1Row3.appendChild(checkInput);
    row3.appendChild(col1Row3);
    
    const row4 = HtmlGenerator.createRow();
    const col1Row4 = HtmlGenerator.createCol(12);
    const commentInput = HtmlGenerator.createInputTextArea('Comment: ', 'inputActionComment');
    col1Row4.appendChild(commentInput);
    row4.appendChild(col1Row4);


    container.appendChild(rowHead);
    container.appendChild(row2);
    container.appendChild(row3);
    container.appendChild(row4);

    return container;
}

function createCheckForm(section) {
    const container = HtmlGenerator.createCol(12);
    const rowHead = HtmlGenerator.createRow();
    const row2 = HtmlGenerator.createRow();
    const row3 = HtmlGenerator.createRow();


    rowHead.classList.add('mb-3');

    const col1Row1 = HtmlGenerator.createCol(null, false);
    col1Row1.classList.add('col-12');
    col1Row1.classList.add('col-sm-12');
    col1Row1.classList.add('col-md-12');
    col1Row1.classList.add('col-lg-9');
    col1Row1.classList.add('col-xl-9');


    const h2 = document.createElement('h2');
    h2.classList.add('source-color');
    h2.classList.add('fs-18');
    h2.classList.add('text-center');
    h2.classList.add('text-sm-center');
    h2.classList.add('text-md-center');
    h2.classList.add('text-lg-left');
    h2.classList.add('text-xl-left');
    h2.innerHTML = 'CHECK';
    col1Row1.appendChild(h2);

    const col2Row1 = HtmlGenerator.createCol(null, false);
    col2Row1.classList.add('col-6');
    col2Row1.classList.add('col-sm-6');
    col2Row1.classList.add('col-md-6');
    col2Row1.classList.add('col-lg-auto');
    col2Row1.classList.add('col-xl-auto');

    const createCMPButton = HtmlGenerator.createSuccessButton('New CMP');
    createCMPButton.addEventListener('click', () => row2.appendChild(createOperationCmp()));
    col2Row1.appendChild(createCMPButton);

    const col3Row1 = HtmlGenerator.createCol(null, false);
    col3Row1.classList.add('col-6');
    col3Row1.classList.add('col-sm-6');
    col3Row1.classList.add('col-md-6');
    col3Row1.classList.add('col-lg-auto');
    col3Row1.classList.add('col-xl-auto');
    col3Row1.classList.add('text-right')
    const deleteButton = HtmlGenerator.createDeleteButton();
    deleteButton.addEventListener('click', () => {
        const deleteSection = confirm('Are you sure you want to delete check?');

        if (deleteSection) {
            section.remove();
            calcSubRuleId();
        }
    });
    col3Row1.appendChild(deleteButton);

    rowHead.appendChild(col1Row1);
    rowHead.appendChild(col2Row1);
    rowHead.appendChild(col3Row1);


    const col1Row2 = HtmlGenerator.createIdText();
    const col2Row2 = HtmlGenerator.createCol(5);
    const col3Row2 = HtmlGenerator.createCol(5);
    const ghostCol = HtmlGenerator.createCol(5);
    const checkInput = HtmlGenerator.createInput('Var Name: ', 'inputCheckVarName');
    col2Row2.appendChild(checkInput);

    const optionsSelect2 = [
        { text: 'Alerts', isSelected: true },
        { text: 'None', value: 'none' },
        { text: 'Alert 1', value: 'alert_1' },
        { text: 'Alert 2', value: 'alert_2' },
        { text: 'Alert 3', value: 'alert_3' },
        { text: 'Alert 4', value: 'alert_4' }
    ];
    const inputSelectAlert = HtmlGenerator.createSelect('Alert ID', 'selectOptionsAlertsId', optionsSelect2);
    col3Row2.appendChild(inputSelectAlert);

    row2.append(col1Row2);
    row2.append(col2Row2);
    row2.append(col3Row2);
    row2.append(ghostCol);

    container.appendChild(rowHead);
    container.appendChild(row2);
    container.appendChild(row3);
    return container;
}

function createOperationCmp() {
    const container = HtmlGenerator.createCol(12);
    const sectionRow = HtmlGenerator.createSection();
    sectionRow.classList.remove('data-section');
    const containerCols = HtmlGenerator.createCol(12);

    const rowHeadTitle = HtmlGenerator.createRow();
    rowHeadTitle.classList.add('mb-3');

    const h2 = document.createElement('h2');
    h2.classList.add('source-color');
    h2.classList.add('fs-18');
    h2.classList.add('text-left');
    h2.innerHTML = 'Operation CMP';

    const colTitle = HtmlGenerator.createCol(6, false);
    colTitle.appendChild(h2);
    rowHeadTitle.appendChild(colTitle);
    containerCols.appendChild(rowHeadTitle);
    
    const buttonCol = HtmlGenerator.createCol(6, false);
    buttonCol.classList.add('text-right');

    const buttonDelete = HtmlGenerator.createDeleteButton();
    buttonDelete.addEventListener('click', () => {
        const confirmation = confirm('Are you sure you want to delete operation CMP?');
        if (confirmation) {
            container.remove();
        }
    });
    buttonCol.appendChild(buttonDelete);

    rowHeadTitle.appendChild(buttonCol);
    
    // inputs

    const row1Inputs = HtmlGenerator.createRow();
    const col1Row1 = HtmlGenerator.createCol(5);
    const col2Row1 = HtmlGenerator.createCol();
    const col3Row1 = HtmlGenerator.createCol(4);

    const input1 = HtmlGenerator.createInput('Var CMP', 'inputVarCMP');
    col1Row1.appendChild(input1);

    const optionsSelect = [
        { text: 'Options', isSelected: true },
        { text: '>', value: '>' },
        { text: '<', value: '<' },
        { text: '=>', value: '=>' },
        { text: '=<', value: '=<' },
        { text: '!>', value: '!>' },
        { text: '!<', value: '!<' },
        { text: '!<=', value: '!<=' },
        { text: '!>=', value: '!>=' },
        { text: '==', value: '==' },
        { text: 'Regex Check', value: 'regex_check' }
    ];

    const input2 = HtmlGenerator.createSelect('IF OP', 'selectOptionVarCMP', optionsSelect);
    col2Row1.appendChild(input2);


    const input3 = HtmlGenerator.createInput('Value CMP', 'inputValueCMP');
    col3Row1.appendChild(input3);

    row1Inputs.appendChild(col1Row1);
    row1Inputs.appendChild(col2Row1);
    row1Inputs.appendChild(col3Row1);
    containerCols.appendChild(row1Inputs);


    const row2Inputs = HtmlGenerator.createRow();
    const col1Row2 = HtmlGenerator.createCol(5);

    const optionsSelect3 = [
        { text: 'Options', isSelected: true },
        { text: 'AND', value: 'and' },
        { text: 'OR', value: 'or' },
        { text: 'XOR', value: 'xor' }
    ];
    const inputSelectLogical = HtmlGenerator.createSelect('Logical OP', 'selectOptionsLogicalOp', optionsSelect3);
    col1Row2.appendChild(inputSelectLogical);

    row2Inputs.appendChild(col1Row2);
    containerCols.appendChild(row2Inputs);

    sectionRow.appendChild(containerCols);
    container.appendChild(sectionRow);
    return container;
}

function createCollectList() {
    const container = HtmlGenerator.createCol(12);

    const rowHead = HtmlGenerator.createRow();
    rowHead.classList.add('mb-3');
    rowHead.classList.add('justify-content-between');

    const rowValue = HtmlGenerator.createRow();

    const col1Row1 = HtmlGenerator.createCol(null, false);
    col1Row1.classList.add('col');
    col1Row1.classList.add('col-sm-4');
    col1Row1.classList.add('col-md-4');
    col1Row1.classList.add('col-lg-4');
    col1Row1.classList.add('col-xl-4');

    const h2 = document.createElement('h2');
    h2.classList.add('source-color');
    h2.classList.add('fs-18');
    h2.classList.add('text-center');
    h2.classList.add('text-sm-center');
    h2.classList.add('text-md-center');
    h2.classList.add('text-lg-left');
    h2.classList.add('text-xl-left');
    h2.innerHTML = 'COLLECT LIST';

    col1Row1.appendChild(h2);

    const col2Row1 = HtmlGenerator.createCol(null, false);
    col2Row1.classList.add('col');
    col2Row1.classList.add('col-sm-4');
    col2Row1.classList.add('col-md-4');
    col2Row1.classList.add('col-lg-4');
    col2Row1.classList.add('col-xl-4');
    col2Row1.classList.add('text-right');
    col2Row1.classList.add('pr-0');

    const newCheckButton = HtmlGenerator.createSuccessButton('New Value');

    newCheckButton.addEventListener('click', () => {
        const col = HtmlGenerator.createCol(12);
        const inputsRow = HtmlGenerator.createRow();

        const colInput = HtmlGenerator.createCol(null, false);
        colInput.classList.add('col-9');
        colInput.classList.add('col-sm-9');
        colInput.classList.add('col-md-9');
        colInput.classList.add('col-lg-11');
        colInput.classList.add('col-xl-11');
        colInput.classList.add('p-0');
        const inputValue = HtmlGenerator.createInput('Value', 'inputCollectValue');
        colInput.appendChild(inputValue);

        const colDelete = HtmlGenerator.createCol(null, false);
        colDelete.classList.add('text-right');
        colDelete.classList.add('p-0');
        const deleteButton = HtmlGenerator.createDeleteButton();
        deleteButton.addEventListener('click', () => {
            const deleteValue = confirm('Are you sure you want to delete value?');

            if (deleteValue) {
                rowValue.removeChild(col); 
            }
        });

        colDelete.appendChild(deleteButton);

        inputsRow.appendChild(colInput);
        inputsRow.appendChild(colDelete);

        col.appendChild(inputsRow);
        rowValue.appendChild(col);
    });

    const col1Row2 = HtmlGenerator.createCol(11);
    col1Row2.classList.add('p-0');
    col2Row1.appendChild(newCheckButton);
    

    rowHead.appendChild(col1Row1);
    rowHead.appendChild(col2Row1);


    const inputValue = HtmlGenerator.createInput('Value', 'inputCollectValue_fisrt');
    col1Row2.appendChild(inputValue);
    rowValue.appendChild(col1Row2);

    // rowHead.appendChild(col3);
    container.appendChild(rowHead);
    container.appendChild(HtmlGenerator.createRow());
    container.appendChild(rowValue);

    return container;
}

function main() {
    // Validar se os campos estão preenchidos
    const operation = document.querySelector('#operationsGroup').value;
    const ruleName = document.querySelector('#ruleName').value;
    const ruleDescription = document.querySelector('#ruleDesc').value;

    jsonRules.rules[0].operation = operation;
    jsonRules.rules[0].value = ruleName;
    jsonRules.rules[0].desc = ruleDescription;


    const sections = document.querySelectorAll('.data-section');

    sections.forEach((section, index) => {
        const colDataRows = section.querySelector('.col-12');
        const rowsData = colDataRows.querySelectorAll('.row');
        const type = rowsData[0].querySelector('h2').textContent;
        const sectionsRule = type==='RULE MAP' ? sections[index] : null;
        const rule = getJsonRules(type, rowsData, sectionsRule);
        jsonRules.rules.push(rule);
    });
    
}

function getJsonRules(type, rowsData, sections) {

    switch(type) {
        case 'COLLECT':
            const jsonCollectRule = {
                "id": "", 
                "id_in": "", 
                "source_info": "", 
                "var_type": "", 
                "main_op": "collect", 
                "value": "", 
                "match_in": "", 
                "desc": ""
            };

            for(let i = 1; i < rowsData.length; i += 1) {
                const row = rowsData[i];
    
                if (i === 1) {
                    jsonCollectRule.id = row.querySelector('#subRule-id').textContent;
                    jsonCollectRule.id_in = row.querySelector('#varName').value;
                    jsonCollectRule.var_type = row.querySelector('#varType').value;
                }
                else if (i === 2) {
                    jsonCollectRule.match_in = row.querySelector('#varMethod').value;
                    jsonCollectRule.value = row.querySelector('#inputValue').value;
                    jsonCollectRule.source_info = row.querySelector('#varSourceInfo').value;
                }
                else if (i === 3) {
                    jsonCollectRule.desc = row.querySelector('#inputDescription').value;
                }
            }
            
            return jsonCollectRule;
        case 'COLLECT MASTER':
            const jsonCollectMasterRule = {
                "id": "", 
                "id_in": "", 
                "source_info": "", 
                "var_type": "", 
                "main_op": "collect_master", 
                "value": "", 
                "match_in": "", 
                "desc": ""
            };

            for(let i = 1; i < rowsData.length; i += 1) {
                const row = rowsData[i];
    
                if (i === 1) {
                    jsonCollectMasterRule.id = row.querySelector('#subRule-id').textContent;
                    jsonCollectMasterRule.id_in = row.querySelector('#varName').value;
                    jsonCollectMasterRule.var_type = row.querySelector('#varType').value;
                }
                else if (i === 2) {
                    jsonCollectMasterRule.match_in = row.querySelector('#varMethod').value;
                    jsonCollectMasterRule.value = row.querySelector('#inputValue').value;
                    jsonCollectMasterRule.source_info = row.querySelector('#varSourceInfo').value;
                }
                else if (i === 3) {
                    jsonCollectMasterRule.desc = row.querySelector('#inputDescription').value;
                }
            }
            
            return jsonCollectMasterRule;
        case "RULE MAP":
            const jsonRuleMap = {
                'id_in': rowsData[2].querySelector('#inputRuleMapVarName').value + '_RM',
                'title': rowsData[2].querySelector('#inputRuleMapTitle').value,
                'main_op': 'rule_map',
                'op_in': 'match_first',
                'collect_group': {
                    'collect_list':[],
                    'aggregate_mode': 'collect_master'
                },
                'collect_source': rowsData[3].querySelector('#inputRuleMapCollectSource').value,
                'id': rowsData[2].querySelector('#subRule-id').textContent,
                'desc': rowsData[3].querySelector('#inputRuleMapDescription').value,
                'rules_in': []
            };

            const sectionCollectList = rowsData[4];

            jsonRuleMap.collect_group.collect_list.push(
                { 'value': sectionCollectList.querySelector('#inputCollectValue_fisrt').value
            });

            const inputsList = sectionCollectList.querySelectorAll('#inputCollectValue');
            inputsList.forEach(collectValue => {
                jsonRuleMap.collect_group.collect_list.push(
                    { 'value': collectValue.value }
                );
            });

            const checkSections = sections.querySelectorAll('.check-data');
            const actionSections = sections.querySelectorAll('.action-data');

            checkSections.forEach(checkSection => {
                const check = {
                    'id': checkSection.querySelector('#subRule-id').textContent,
                    'id_in': checkSection.querySelector('#inputCheckVarName').value + '_CK',
                    'main_op': 'check',
                    'check_list': []
                };

                const hasAlert = checkSection.querySelector('#selectOptionsAlertsId').value !== 'none' ? true : false;
                
                if (hasAlert) {
                    check['alert_id'] = checkSection.querySelector('#selectOptionsAlertsId').value;
                }

                const sectionsCMP = checkSection.querySelectorAll('section');
                sectionsCMP.forEach(cmp => {
                    const operation = {
                        'src_cmp': `$${cmp.querySelector('#inputVarCMP').value}`,
                        'if_op': cmp.querySelector('#selectOptionVarCMP').value,
                        'value_cmp': cmp.querySelector('#inputValueCMP').value,
                        'logical_op': cmp.querySelector('#selectOptionsLogicalOp').value
                    };
                    check.check_list.push(operation);
                });

                jsonRuleMap.rules_in.push(check);
            });

            actionSections.forEach(actionSection => {
                const action = {
                    'id': actionSection.querySelector('#subRule-id').textContent,
                    'id_in': `${actionSection.querySelector('#inputActionVarName').value}_ACT`,
                    'main_op': 'action',
                    'alert_id_cmp': actionSection.querySelector('#selectOptionsAlertsType').value,
                    'op_in': actionSection.querySelector('#selectOptionsAlertNotification').value,
                    'msg': actionSection.querySelector('#inputActionComment').value
                };

                jsonRuleMap.rules_in.push(action);
            });
            
            console.log(jsonRuleMap);
            return jsonRuleMap;
        }
}

function createForm(ruleType) {
    const $body = document.querySelector('.container');

    switch(ruleType) {
        case 'collect':
            createCollect($body, 'Collect');
            break;
        case 'collect-master':
            createCollect($body, 'Collect Master');
            break;
        case 'rule-map':
            createRuleMap($body);
            break;
    }

    calcSubRuleId();
}

function calcSubRuleId() {
    const subRules = document.querySelectorAll('#subRule-id');
    let id = 10;

    subRules.forEach(span => {
        span.innerHTML = '1.' + id;
        id += 10;
    });
}

function createCollect(body, typeCollect) {
    const section = HtmlGenerator.createSection();
    const divCol = HtmlGenerator.createCol(12);

    const rowHead = createSectionHead(typeCollect, body, section);

    const row1Inputs = HtmlGenerator.createRow();

    const colIdRow1 = HtmlGenerator.createIdText();
    
    const colInputRow1 = HtmlGenerator.createCol(5);

    colInputRow1.appendChild(HtmlGenerator.createInput('Var Name:', 'varName')); 

    const colSelectRow1 = HtmlGenerator.createCol(5);
    const optionsSelect1 = [
        { text: 'Var type', isSelected: true },
        { text: 'Integer', value: 'int' },
        { text: 'String', value: 'string' },
    ];
    colSelectRow1.appendChild(HtmlGenerator.createSelect('Var Type', 'varType', optionsSelect1));

    row1Inputs.appendChild(colIdRow1); 
    row1Inputs.appendChild(colInputRow1); 
    row1Inputs.appendChild(colSelectRow1); 

    const row2Inputs = HtmlGenerator.createRow();

    const colSelectRow2 = HtmlGenerator.createCol(3);
    const optionsSelect2 = [
        { text: 'Method', isSelected: true },
        { text: 'Field', value: 'field' },
        { text: 'Regex', value: 'regex' },
        { text: 'Snmp Value', value: 'snmp_val' }
    ];
    colSelectRow2.appendChild(HtmlGenerator.createSelect('Method', 'varMethod', optionsSelect2));

    const colInputRow2 = HtmlGenerator.createCol();
    colInputRow2.appendChild(HtmlGenerator.createInput('Value:', 'inputValue')); 
    
    const colSelect2Row2 = HtmlGenerator.createCol(3);
    const options2Select2 = [
        { text: 'Source info', isSelected: true },
        { text: 'Node', value: 'node' },
        { text: 'Interface', value: 'interface' }
    ];
    colSelect2Row2.appendChild(HtmlGenerator.createSelect('Source Info', 'varSourceInfo', options2Select2));

    row2Inputs.appendChild(colSelectRow2);
    row2Inputs.appendChild(colInputRow2);
    row2Inputs.appendChild(colSelect2Row2);


    const row3Inputs = HtmlGenerator.createRow();

    const colInputRow3 = HtmlGenerator.createCol();
    colInputRow3.appendChild(HtmlGenerator.createInput('Description:', 'inputDescription'));

    row3Inputs.appendChild(colInputRow3);
    
    divCol.appendChild(rowHead);
    divCol.appendChild(row1Inputs);
    divCol.appendChild(row2Inputs);
    divCol.appendChild(row3Inputs);

    section.appendChild(divCol);
    body.appendChild(section);
}

function createRuleMap(body) {
    const section = HtmlGenerator.createSection();
    const divCol = HtmlGenerator.createCol(12);
    const rowHead = createSectionHeadRuleMap(body, section);

    // row 1
    const row1Inputs = HtmlGenerator.createRow();

    const colIdRow1 = HtmlGenerator.createIdText();
    row1Inputs.appendChild(colIdRow1);
    
    const col2InputRow1 = HtmlGenerator.createCol(5);
    col2InputRow1.appendChild(HtmlGenerator.createInput('Title: ', 'inputRuleMapTitle')); 
    row1Inputs.appendChild(col2InputRow1);
    
    const col3InputRow1 = HtmlGenerator.createCol(5);
    col3InputRow1.appendChild(HtmlGenerator.createInput('Var Name: ', 'inputRuleMapVarName')); 
    row1Inputs.appendChild(col3InputRow1);

    // row 2
    const row2Inputs = HtmlGenerator.createRow();

    const col1InputRow2 = HtmlGenerator.createCol(7);
    col1InputRow2.appendChild(HtmlGenerator.createInput('Description: ', 'inputRuleMapDescription')); 
    row2Inputs.appendChild(col1InputRow2);

    const col2InputRow2 = HtmlGenerator.createCol(5);
    col2InputRow2.appendChild(HtmlGenerator.createInput('Collect Source: ', 'inputRuleMapCollectSource')); 
    row2Inputs.appendChild(col2InputRow2);

    // row 3 Collect List
    const collectListSection = HtmlGenerator.createSection();
    collectListSection.classList.remove('data-section');
    collectListSection.appendChild(createCollectList());
    

    divCol.appendChild(rowHead);
    divCol.appendChild(row1Inputs);
    divCol.appendChild(row2Inputs);
    divCol.appendChild(collectListSection);

    section.appendChild(divCol);
    body.appendChild(section);
}

function downloadRule() {
    const filename = document.querySelector('#fileName').value || 'rule_withoutname.json';
    const element = document.createElement('a');

    element.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(jsonRules)));
    element.setAttribute('download', filename+'.json');
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();  
    document.body.removeChild(element);
}

function saveAndDownload() {
    main();
    downloadRule();
}