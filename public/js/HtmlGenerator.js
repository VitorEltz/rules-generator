
class HtmlGenerator {

    static createMainDiv() {
        const divMain = document.createElement('div');
        divMain.classList.add('input-group');
        divMain.classList.add('mb-3');

        return divMain;
    }

    static createPreprendDiv() {
        const div = document.createElement('div');
        div.classList.add('input-group-prepend');

        return div;
    }

    
    
    static createSection () {

        const section = document.createElement('section');
        section.classList.add('data-section');
        section.classList.add('row');
        section.classList.add('border');
        section.classList.add('border-dark');
        section.classList.add('rounded');
        section.classList.add('py-2');
        section.classList.add('px-4');
        section.classList.add('mb-4');
        
        return section;
    }

    static createRow() {
        const div = document.createElement('div');
        div.classList.add('row')
        return div;
    }

    static createCol(size = null, isMobileMode = true) {
        const div = document.createElement('div');

        if (isMobileMode) {
            div.classList.add('col-12');
            div.classList.add('col-sm-12');
            div.classList.add('col-md-12');
            div.classList.add(size ? `col-lg-${size}` : 'col-lg-auto');
            div.classList.add(size ? `col-xl-${size}` : 'col-xl-auto');
        } else {
            div.classList.add(size ? `col-${size}` : 'col');
        }
        
        return div;
    }

    static createTitleRow(title) {
        const row = this.createRow();
        row.classList.add('justify-content-between');
        row.classList.add('mb-3');

        const col1 = this.createCol(6, false);
        // const col2 = this.createCol(4);
        
        const h2 = document.createElement('h2');
        h2.classList.add('source-color');
        h2.classList.add('fs-18');
        h2.classList.add('text-left');
        h2.innerHTML = title.toUpperCase();
        col1.appendChild(h2);

        
        row.appendChild(col1);
        // row.appendChild(col2);

        return row;
    }
    
    static createDeleteButton() {
        const button = document.createElement('button');
        button.classList.add('btn');
        button.classList.add('btn-sm');
        button.classList.add('btn-danger');
        // button.classList.add('text-right');
        button.innerHTML = 'Delete';

        return button;
    }

    static createSuccessButton(title) {
        const button = document.createElement('button');
        button.classList.add('btn');
        button.classList.add('btn-sm');
        button.classList.add('btn-success');
        button.innerHTML = title;

        return button;
    }


    static createInput(title, inputId) {
        const divMain = this.createMainDiv();
        const div = this.createPreprendDiv()
        
        const span = document.createElement('span');
        span.classList.add('input-group-text');
        span.innerHTML = title;

        div.appendChild(span);

        const input = document.createElement('input');
        input.type = 'text';
        input.classList.add('form-control');
        input.id = inputId;

        divMain.appendChild(div);
        divMain.appendChild(input);

        return divMain;
    }

    static createInputTextArea(title, inputId) {
        const divMain = this.createMainDiv();
        divMain.classList.remove('input-group');
        divMain.classList.add('form-group');
        
        const label = document.createElement('label');
        label.htmlFor = inputId;
        label.innerHTML = title;

        const input = document.createElement('textarea');
        input.classList.add('form-control');
        input.id = inputId;

        divMain.appendChild(label);
        divMain.appendChild(input);

        return divMain;
    }

    static createIdText() {
        const col = this.createCol();
        col.classList.add('mb-3');
        col.classList.add('mb-sm-3');
        col.classList.add('mb-md-4');
        col.classList.add('text-center');

        const div = document.createElement('div');
        div.classList.add('id-source');

        const span = document.createElement('span');
        span.id = 'subRule-id';
        span.classList.add('source-color');

        div.innerHTML = 'ID: ';
        div.appendChild(span);

        col.appendChild(div);

        return col;
    }

    static createSelect(title, selectId, options) {
        const mainDiv = this.createMainDiv();
        const div = this.createPreprendDiv();

        const label = document.createElement('label');
        label.classList.add('input-group-text');
        label.htmlFor = selectId;
        label.innerHTML = title;

        div.appendChild(label);

        const select = document.createElement('select');
        select.classList.add('custom-select');
        select.id = selectId;

        options.forEach(optionItem => {
            const option = document.createElement('option');

            if (optionItem.isSelected) {
                option.selected = true;
                option.disabled = true;
                option.innerHTML = optionItem.text;

                select.appendChild(option);
            } else {
                option.value = optionItem.value;
                option.innerHTML = optionItem.text;

                select.appendChild(option);
            }
        });
        
        mainDiv.appendChild(div);
        mainDiv.appendChild(select);

        return mainDiv;
    }
}
